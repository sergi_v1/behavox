package com.vladykin.behavox.elastic;

import com.vladykin.behavox.email.Email;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.junit.jupiter.api.Test;

import static com.vladykin.behavox.util.UncheckedException.unchecked;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ElasticStoreTest {

    @Test
    void testStore() throws InterruptedException {
        BulkResponse response = new BulkResponse(new BulkItemResponse[0], 0);

        int threads = 20;
        ExecutorService exec = Executors.newFixedThreadPool(threads);

        AtomicInteger parallelTasks = new AtomicInteger();
        AtomicInteger stored = new AtomicInteger();

        int maxParallelBatches = threads - 3;

        ElasticStore s = new ElasticStore(null, 0, 1, maxParallelBatches) {
            @Override
            protected void bulkAsync(int batch, int batchSize, BulkRequest request, ElasticFuture<BulkResponse> future) {
                assertTrue(batchSize > 0);
                assertTrue(batchSize <= 3);
                assertTrue(parallelTasks.incrementAndGet() <= maxParallelBatches);

                exec.submit(() -> {
                    Thread.sleep(1);
                    stored.addAndGet(batchSize);
                    parallelTasks.decrementAndGet();
                    future.complete(response);
                    return null;
                });
            }
        };

        Email email = new Email();

        email.setMessageId("1");
        email.setSubject("Test");

        StringBuilder bodyBuilder = new StringBuilder();

        for (int i = 0; i < 100; i++)
            bodyBuilder.append("bla");

        email.setBody(bodyBuilder.toString());

        int emailsCount = 100_000;

        assertEquals(emailsCount, s.storeEmails(new Iterator<Email>() {
            int current;

            @Override
            public boolean hasNext() {
                return current < emailsCount;
            }

            @Override
            public Email next() {
                if (!hasNext())
                    throw new NoSuchElementException();

                current++;
                return email;
            }
        }));

        assertEquals(emailsCount, stored.get());

        exec.shutdownNow();
        exec.awaitTermination(1, TimeUnit.SECONDS);
    }

    @Test
    void testAssertError() {
        testError(new AssertionError("Oops!"));
    }

    @Test
    void testException() {
        testError(new IllegalStateException("Oops!"));
    }

    private void testError(Throwable e) {
        ElasticStore s = new ElasticStore(null, 0, 0, 5) {
            @Override
            protected void bulkAsync(int batch, int batchSize, BulkRequest request, ElasticFuture<BulkResponse> future) {
                if (batch != 1)
                    return;

                new Thread(() -> {
                    try {
                        Thread.sleep(10);
                    }
                    catch (InterruptedException e) {
                        throw unchecked(e);
                    }
                    future.completeExceptionally(e);
                }).start();
            }
        };

        Email email = new Email();

        email.setMessageId("1");
        email.setSubject("Test");

        Class<? extends Throwable> errType = e instanceof Exception ? ElasticException.class : Error.class;

        assertThrows(errType, () ->
            s.storeEmails(asList(email, email, email).iterator()));
    }
}
