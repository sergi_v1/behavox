package com.vladykin.behavox.email;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import org.junit.jupiter.api.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EmailJsonTest {
    @Test
    void testJson() throws IOException {
        Email email = new Email();

        email.setMessageId("-100500");
        email.setDate(new Date());
        email.setFrom(asList("me@com", "not.me"));
        email.setTo(singletonList("you@com"));

        email.setSubject("Hi!");
        email.setBody("Good day today!");

        email.setCompression("gzip");
        email.setOriginal(new byte[]{1,2,3,4,5,6});

        String json = EmailJson.toJsonString(email);

        for (String field: EmailJson.FIELDS)
            assertTrue(json.contains('"' + field + '"'));

        String[] values = {
            "-100500", "me@com", "not.me", "you@com", "Hi!", "Good day today!", "gzip",
            new String(Base64.getEncoder().encode(new byte[]{1,2,3,4,5,6}), UTF_8)
        };

        for (String val: values)
            assertTrue(json.contains('"' + val + '"'));

        assertTrue(json.contains(String.valueOf(email.getDate().getTime())));
    }
}
