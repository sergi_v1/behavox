package com.vladykin.behavox.email;

import com.vladykin.behavox.util.CompressUtils;
import com.vladykin.behavox.util.CompressingInputStream;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.james.mime4j.MimeException;
import org.junit.jupiter.api.Test;

import static com.vladykin.behavox.util.CompressUtils.iteratorOverArchive;
import static java.util.Collections.singletonList;
import static org.apache.commons.compress.compressors.CompressorStreamFactory.ZSTANDARD;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmailMimeParserTest {

    private static InputStream readFile(String file) throws FileNotFoundException {
        return new BufferedInputStream(new FileInputStream(file));
    }

    @Test
    void testSimpleEmail() throws IOException, MimeException {
        String emailFile = "/Users/sergi/Downloads/maildir/causholli-m/deleted_items/2.";

        try (InputStream in = readFile(emailFile)) {
            Email email = EmailMimeParser.parse(in, ZSTANDARD);

            assertEquals("<6653353.1075853152211.JavaMail.evans@thyme>", email.getMessageId());
            assertEquals(new Date("Mon, 29 Oct 2001 13:40:05 -0800 (PST)"), email.getDate());
            assertEquals(singletonList("morela.hernandez@enron.com"), email.getFrom());
            assertEquals(singletonList("monika.causholli@enron.com"), email.getTo());
            assertEquals("World market pulp inventories fall nearly 10 percent", email.getSubject());
            String body = email.getBody().trim();
            assertTrue(body.startsWith(":-)") && body.endsWith("5 points to 95 percent, PPPC reported."));
            assertEquals(ZSTANDARD, email.getCompression());
            assertTrue(email.getOriginal().length > 0);
        }
    }

    @Test
    void testAllEnronEmails() throws Exception {
        try (InputStream in = readFile("/Users/sergi/Downloads/enron_mail_20150507.tar")) {
            AtomicInteger cnt = new AtomicInteger();

            Iterator<Email> iter = iteratorOverArchive(null, in, (mime) -> {
                try {
                    CompressingInputStream copy = new CompressingInputStream(mime, null);

                    String compressionMethod = ZSTANDARD;

                    Email email = EmailMimeParser.parse(copy, true, compressionMethod);
                    byte[] original = copy.getCompressedBytes();

                    assertArrayEquals(original, CompressUtils.uncompress(compressionMethod, email.getOriginal()));
                    assertTrue(email.getOriginal().length < original.length); // Compressed size must be smaller.

                    return email;
                }
                catch (IOException | CompressorException e) {
                    throw new IllegalStateException(e);
                }
            });

            while (iter.hasNext()) {
                Email email = iter.next();

                assertNotNull(email.getMessageId());
                assertNotNull(email.getDate());
                assertFalse(email.getFrom().isEmpty());
                assertTrue(email.getTo() == null || !email.getTo().isEmpty());
                assertFalse(email.getSubject().isEmpty());
                assertFalse(email.getBody().isEmpty());

                if (cnt.incrementAndGet() % 1000 == 0)
                    System.out.println(cnt);
            }
        }
    }
}
