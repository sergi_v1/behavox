package com.vladykin.behavox.util;

import java.io.IOException;

public interface ConsumerIO<T> {
    void accept(T t) throws IOException;
}
