package com.vladykin.behavox.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

import static java.util.Objects.requireNonNull;

public class CompressingInputStream extends InputStream {

    private final InputStream in;
    private final ByteArrayOutputStream bytes;
    private OutputStream compressing;

    /**
     * @param in The original input stream.
     * @param compressionMethod Compression method from {@link CompressorStreamFactory}
     *                          or {@code null} for no compression.
     */
    public CompressingInputStream(InputStream in, String compressionMethod) throws IOException {
        this.in = requireNonNull(in);

        bytes = new ByteArrayOutputStream();

        try {
            compressing = compressionMethod == null ? bytes:
                CompressorStreamFactory.getSingleton().createCompressorOutputStream(compressionMethod, bytes);
        }
        catch (CompressorException e) {
            throw new IOException(e);
        }
    }

    @Override
    public int read() throws IOException {
        int b = in.read();

        if (b > -1)
            compressing.write(b);

        return b;
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        len = in.read(b, off, len);

        if (len > 0)
            compressing.write(b, off, len);

        return len;
    }

    public byte[] getCompressedBytes() throws IOException {
        compressing.close();
        compressing = null;

        return bytes.toByteArray();
    }
}
