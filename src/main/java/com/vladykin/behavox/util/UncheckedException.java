package com.vladykin.behavox.util;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public class UncheckedException extends RuntimeException {

    public static RuntimeException unchecked(Exception e) {
        if (e instanceof IOException)
            return new UncheckedIOException(e.getMessage(), (IOException)e);

        return new UncheckedException(e);
    }

    public static void throwUnchecked(Throwable error) {
        throwUnchecked(error, null);
    }

    public static void throwUnchecked(Throwable error, Function<Exception, RuntimeException> wrap) {
        requireNonNull(error);

        if (error instanceof Exception)
            throw wrap != null ? wrap.apply((Exception)error) : unchecked((Exception)error);

        throw error(error);
    }

    private static Error error(Throwable e) {
        if (e instanceof OutOfMemoryError)
            return (Error)e;

        if (e instanceof AssertionError)
            return new AssertionError(e.getMessage(), e);

        return new Error(e.getMessage(), e);
    }

    private UncheckedException(Throwable e) {
        super(e.getMessage(), e);
    }
}
