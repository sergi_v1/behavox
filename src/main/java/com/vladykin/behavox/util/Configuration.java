package com.vladykin.behavox.util;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;

import static java.util.Objects.requireNonNull;

public abstract class Configuration extends AbstractModule {

    private final Properties properties;

    private final Set<String> knownPropertyNames = new HashSet<>();

    protected Configuration(Properties properties) {
        this.properties = properties;
    }

    public static Properties loadFromXml(String... files) throws IOException {
        if (files == null || files.length == 0)
            return null;

        Properties properties = null;

        for (String file : files) {
            properties = new Properties(properties);

            properties.loadFromXML(
                new BufferedInputStream(
                new FileInputStream(file)));
        }

        return properties;
    }

    protected void set(String name, String defaultValue) {
        set(String.class, name, defaultValue, Function.identity());
    }

    protected void set(String name, int defaultValue) {
        set(int.class, name, defaultValue, Integer::parseInt);
    }

    private <T> void set(Class<T> type, String name, T v, Function<String, T> convert) {
        requireNonNull(name, "Property name is null.");

        if (!knownPropertyNames.add(name))
            throw new IllegalArgumentException("Property name duplicate: " + name);

        if (properties != null) {
            String x = properties.getProperty(name);

            if (x != null)
                v = convert.apply(x);
        }

        bind(type).annotatedWith(Names.named(name)).toInstance(v);
    }
}
