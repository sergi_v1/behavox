package com.vladykin.behavox.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Function;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.compress.utils.IOUtils;

import static com.vladykin.behavox.util.UncheckedException.unchecked;
import static java.util.Objects.requireNonNull;

public class CompressUtils {

    /**
     * @param archiveType Archive type or {@code null} to detect automatically.
     * @param archive Streamed archive.
     * @param fileHandler Handler to be called for each uncompressed file.
     */
    public static <R> Iterator<R> iteratorOverArchive(
        String archiveType,
        InputStream archive,
        Function<InputStream, R> fileHandler
    ) throws ArchiveException {
        requireNonNull(archive);
        requireNonNull(fileHandler);

        if (archiveType == null)
            archiveType = ArchiveStreamFactory.detect(archive);

        ArchiveInputStream in = new ArchiveStreamFactory().createArchiveInputStream(archiveType, archive);

        class ArchiveIterator implements Iterator<R> {
            private boolean hasNext;

            @Override
            public boolean hasNext() {
                while (!hasNext) {
                    ArchiveEntry next;

                    try {
                        next = in.getNextEntry();
                    }
                    catch (IOException e) {
                        throw unchecked(e);
                    }

                    if (next == null)
                        return false;

                    hasNext = !next.isDirectory();
                }
                return true;
            }

            @Override
            public R next() {
                if (!hasNext())
                    throw new NoSuchElementException();

                R result = fileHandler.apply(in);
                hasNext = false;
                return result;
            }
        }

        return new ArchiveIterator();
    }

    public static byte[] uncompress(String compressionMethod, byte[] compressed) throws CompressorException, IOException {
        requireNonNull(compressed);

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        IOUtils.copy(uncompress(compressionMethod, new ByteArrayInputStream(compressed)), out);

        return out.toByteArray();
    }

    public static InputStream uncompress(String compressionMethod, InputStream compressed) throws CompressorException {
        requireNonNull(compressed);

        if (compressionMethod == null)
            compressionMethod = CompressorStreamFactory.detect(compressed);

        return CompressorStreamFactory.getSingleton()
            .createCompressorInputStream(compressionMethod, compressed);
    }

    public static InputStream uncompressIfNeeded(InputStream mayBeCompressed) throws CompressorException {
        requireNonNull(mayBeCompressed);

        String compressionMethod;

        try {
            compressionMethod = CompressorStreamFactory.detect(mayBeCompressed);
        }
        catch (CompressorException e) {
            if (e.getCause() == null) {
                assert e.getMessage().startsWith("No Compressor found"): e.getMessage();

                return mayBeCompressed;
            }

            throw e;
        }

        return CompressorStreamFactory.getSingleton()
            .createCompressorInputStream(compressionMethod, mayBeCompressed);
    }

    private CompressUtils() {}
}
