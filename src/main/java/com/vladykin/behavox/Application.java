package com.vladykin.behavox;

import com.google.inject.Guice;
import com.vladykin.behavox.elastic.ElasticStore;
import com.vladykin.behavox.util.Configuration;
import com.vladykin.behavox.web.WebServer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static com.vladykin.behavox.util.Configuration.loadFromXml;

public class Application {

    private static final Log log = LogFactory.getLog(Application.class);

    public static void main(String[] configFiles) throws Exception {
        log.info("Starting...");

        Configuration cfg = new Configuration(loadFromXml(configFiles)) {
            @Override public void configure() {
                set(WebServer.PORT, 8080);

                set(ElasticStore.HOST, "127.0.0.1");
                set(ElasticStore.PORT, 9200);

                set(ElasticStore.BATCH_SIZE_KB, 128);
                set(ElasticStore.MAX_PARALLEL_BATCHES, 1000);
            }
        };

        Guice.createInjector(cfg, new WebServer.Module())
            .getInstance(WebServer.class).start();

        log.info("Started.");
    }
}
