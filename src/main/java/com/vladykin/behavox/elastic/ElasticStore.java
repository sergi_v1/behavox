package com.vladykin.behavox.elastic;

import com.vladykin.behavox.email.Email;
import com.vladykin.behavox.email.EmailJson;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import static com.vladykin.behavox.elastic.email.EmailQueryHelpers.buildQuery;
import static com.vladykin.behavox.elastic.email.EmailQueryHelpers.containsWordsQuery;
import static com.vladykin.behavox.elastic.email.EmailQueryHelpers.datesRangeQuery;
import static com.vladykin.behavox.elastic.email.EmailQueryHelpers.participantsQuery;
import static com.vladykin.behavox.util.UncheckedException.throwUnchecked;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.elasticsearch.client.Requests.indexRequest;

@Singleton
public class ElasticStore {

    public static final String HOST = "elastic_host";
    public static final String PORT = "elastic_port";
    public static final String BATCH_SIZE_KB = "elastic_batch_size_kb";
    public static final String MAX_PARALLEL_BATCHES = "elastic_max_parallel_batches";

    private static final String EMAIL_INDEX = "email";
    private static final String EMAIL_TYPE = "email";

    private static final Log log = LogFactory.getLog(ElasticStore.class);

    private final RestHighLevelClient client;

    private final Semaphore parallelBatches;

    private final int batchSizeBytes;

    @Inject
    public ElasticStore(
        @Named(HOST) String host,
        @Named(PORT) int port,
        @Named(BATCH_SIZE_KB) int batchSizeKB,
        @Named(MAX_PARALLEL_BATCHES) int maxParallelBatches
    ) {
        assert maxParallelBatches > 0: maxParallelBatches;

        this.batchSizeBytes = batchSizeKB * 1024;
        this.parallelBatches = new Semaphore(maxParallelBatches);

        if (host == null)
            client = null; // For tests.
        else {
            HttpHost httpHost = new HttpHost(host, port, "http");

            client = new RestHighLevelClient(RestClient.builder(httpHost));

            if (log.isInfoEnabled())
                log.info("Started for " + httpHost);
        }
    }

    /**
     * @param batch Order number of the given batch.
     * @param batchSize Number of emails in the given batch.
     * @param request Bulk request.
     * @param future Future to complete.
     */
    protected void bulkAsync(int batch, int batchSize, BulkRequest request, ElasticFuture<BulkResponse> future) {
        client.bulkAsync(request, future);
    }

    private static void throwIfAny(AtomicReference<Throwable> error) {
        Throwable e = error.get();

        if (e != null)
            throwUnchecked(e, ElasticException::new);
    }

    /**
     * @param emails Iterator returning emails.
     * @return Number of processed emails.
     */
    public int storeEmails(Iterator<Email> emails) {
        AtomicReference<Throwable> error = new AtomicReference<>();
        Semaphore completedBatches = new Semaphore(0);

        int allEmailsCount = 0;
        int batchedEmailsCount = 0;
        int batchesCount = 0;

        BulkRequest request = new BulkRequest();

        try {
            while (emails.hasNext()) {
                Email email = emails.next();

                if (log.isDebugEnabled())
                    log.debug("Adding email to batch:\n" + email.toString());

                request.add(
                    indexRequest(EMAIL_INDEX)
                        .type(EMAIL_TYPE)
                        .id(requireNonNull(email.getMessageId()))
                        .source(EmailJson.buildJson(email))
                );

                allEmailsCount++;
                batchedEmailsCount++;

                throwIfAny(error);

                if (request.estimatedSizeInBytes() >= batchSizeBytes || !emails.hasNext()) {
                    parallelBatches.acquire();

                    ElasticFuture<BulkResponse> future = new ElasticFuture<>();
                    future.whenComplete((r, e) -> {
                        parallelBatches.release();
                        completedBatches.release();

                        if (e == null && r.hasFailures())
                            e = new ElasticException(r.buildFailureMessage());

                        if (e != null)
                            error.compareAndSet(null, e);
                    });

                    if (log.isDebugEnabled())
                        log.debug("Sending batch " + batchesCount);

                    bulkAsync(batchesCount, batchedEmailsCount, request, future);

                    batchesCount++;
                    batchedEmailsCount = 0;
                    request = new BulkRequest();
                }
            }

            // Await for all batches to complete.
            while (!completedBatches.tryAcquire(batchesCount, 50, MILLISECONDS))
                throwIfAny(error);

            throwIfAny(error);
        }
        catch (IOException | InterruptedException e) {
            throw new ElasticException(e);
        }

        return allEmailsCount;
    }

    @SuppressWarnings("unchecked")
    public CompletableFuture<Map<String,Object>> queryEmailStatistics(
        Date fromTime,
        Date toTime,
        List<String> participants,
        List<String> containsWords,
        ElasticStatistics statistics
    ) {
        requireNonNull(statistics);

        SearchSourceBuilder builder = new SearchSourceBuilder().query(buildQuery(
            datesRangeQuery(fromTime, toTime),
            participantsQuery(participants),
            containsWordsQuery(containsWords)
        ));

        builder.aggregation(statistics.aggregation());

        SearchRequest req = new SearchRequest(EMAIL_INDEX)
            .types(EMAIL_TYPE)
            .searchType(SearchType.DFS_QUERY_THEN_FETCH)
            .source(builder);

        ElasticFuture<SearchResponse> future = new ElasticFuture<>();
        client.searchAsync(req, future);

        return future.thenApply(statistics::result);
    }
}
