package com.vladykin.behavox.elastic;

import java.util.concurrent.CompletableFuture;
import org.elasticsearch.action.ActionListener;

public class ElasticFuture<R> extends CompletableFuture<R> implements ActionListener<R> {

    @Override
    public void onResponse(R r) {
        complete(r);
    }

    @Override
    public void onFailure(Exception e) {
        completeExceptionally(e);
    }
}
