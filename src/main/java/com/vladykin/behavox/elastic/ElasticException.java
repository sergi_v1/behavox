package com.vladykin.behavox.elastic;

public class ElasticException extends RuntimeException {

    public ElasticException(String message) {
        super(message);
    }

    public ElasticException(Throwable cause) {
        super(cause.getMessage(), cause);
    }
}
