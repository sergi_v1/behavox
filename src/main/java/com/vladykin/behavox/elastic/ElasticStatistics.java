package com.vladykin.behavox.elastic;

import java.util.Map;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.aggregations.AggregationBuilder;

public interface ElasticStatistics {

    String name();

    AggregationBuilder aggregation();

    Map<String,Object> result(SearchResponse response);
}
