package com.vladykin.behavox.elastic.email;

import com.vladykin.behavox.elastic.ElasticStatistics;
import java.util.Map;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;

import static java.util.Collections.singletonMap;
import static org.elasticsearch.search.aggregations.AggregationBuilders.avg;

public enum SimpleEmailStatistics implements ElasticStatistics {

    AVG_BODY_LENGTH {
        @Override
        public AggregationBuilder aggregation() {
            return avg("average").script(new Script("doc['body'].length"));
        }

        @Override
        public Map<String,Object> result(SearchResponse response) {
            Avg agg = response.getAggregations().get("average");
            return singletonMap("avg", agg.getValue());
        }
    }
}
