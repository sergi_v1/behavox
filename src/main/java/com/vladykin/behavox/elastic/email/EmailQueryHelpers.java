package com.vladykin.behavox.elastic.email;

import com.vladykin.behavox.email.EmailJson;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;

import static java.util.stream.Collectors.toList;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;
import static org.elasticsearch.index.query.QueryBuilders.termsQuery;

public class EmailQueryHelpers {

    public static RangeQueryBuilder datesRangeQuery(Date fromDate, Date toDate) {
        if (fromDate == null && toDate == null)
            return null;

        RangeQueryBuilder qb = rangeQuery(EmailJson.DATE);

        if (fromDate != null)
            qb.gte(fromDate.getTime());

        if (toDate != null)
            qb.lte(toDate.getTime());

        return qb;
    }

    public static QueryBuilder participantsQuery(List<String> participants) {
        if (participants == null || participants.isEmpty())
            return null;

        String[] terms = participants.toArray(new String[participants.size()]);

        return boolQuery()
            .should(termsQuery(EmailJson.FROM, terms))
            .should(termsQuery(EmailJson.TO, terms))
            .minimumShouldMatch(1);
    }

    public static QueryBuilder containsWordsQuery(List<String> words) {
        if (words == null || words.isEmpty())
            return null;

        String[] terms = words.toArray(new String[words.size()]);

        return boolQuery()
            .should(termsQuery(EmailJson.SUBJ, terms))
            .should(termsQuery(EmailJson.BODY, terms))
            .minimumShouldMatch(1);
    }

    public static QueryBuilder buildQuery(QueryBuilder... builders) {
        List<QueryBuilder> list = Stream.of(builders).filter(Objects::nonNull).collect(toList());

        if (list.isEmpty())
            return matchAllQuery();

        if (list.size() == 1)
            return list.get(0);

        BoolQueryBuilder bool = boolQuery();
        list.forEach(bool::must);
        return bool;
    }
}
