package com.vladykin.behavox.web.servlets;

import com.vladykin.behavox.elastic.ElasticStore;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.vladykin.behavox.email.EmailMimeParser.parse;
import static com.vladykin.behavox.util.CompressUtils.iteratorOverArchive;
import static com.vladykin.behavox.util.CompressUtils.uncompressIfNeeded;
import static com.vladykin.behavox.util.UncheckedException.unchecked;
import static org.apache.commons.compress.archivers.ArchiveStreamFactory.TAR;
import static org.apache.commons.compress.compressors.CompressorStreamFactory.ZSTANDARD;

@Singleton
public class UploadServlet extends JsonServlet {

    @Inject
    private ElasticStore elasticStore;

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse res) {
        try {
            // Any combination of tar, tar.gz, tar.bz2, etc...
            InputStream in = uncompressIfNeeded(new BufferedInputStream(req.getInputStream()));

            int storedEmails = elasticStore.storeEmails(iteratorOverArchive(TAR, in, (email) -> {
                try {
                    return parse(email, ZSTANDARD);
                }
                catch (IOException e) {
                    throw unchecked(e);
                }
            }));

            respond(req, res, (b) -> b.field("storedEmails", storedEmails));
        }
        catch (Exception e) {
            respond(req, res, e);
        }
    }
}
