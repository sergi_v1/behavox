package com.vladykin.behavox.web.servlets;

import com.vladykin.behavox.elastic.ElasticStatistics;
import com.vladykin.behavox.elastic.ElasticStore;
import com.vladykin.behavox.elastic.email.SimpleEmailStatistics;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.vladykin.behavox.util.UncheckedException.unchecked;
import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

@Singleton
public class ElasticQueryServlet extends JsonServlet {

    private static final ThreadLocal<DateFormat> dateFormat =
        ThreadLocal.withInitial(() -> new SimpleDateFormat("dd.MM.yyyy"));

    @Inject
    private ElasticStore elasticStore;

    private static Date date(String date) {
        try {
            return date == null ? null : dateFormat.get().parse(date);
        }
        catch (ParseException e) {
            throw unchecked(e);
        }
    }

    private static List<String> list(String[] str) {
        return str == null ? null : asList(str);
    }

    private static ElasticStatistics statistics(String stat) {
        requireNonNull(stat, "Parameter `stat` is mandatory.");
        return SimpleEmailStatistics.valueOf(stat.toUpperCase());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) {
        String fromDate = req.getParameter("fromDate");
        String toDate = req.getParameter("toDate");
        String[] participants = req.getParameterValues("participants");
        String[] words = req.getParameterValues("words");
        String stat = req.getParameter("stat");

        try {
            ElasticStatistics statistics = statistics(stat);

            CompletableFuture<Map<String,Object>> future = elasticStore.queryEmailStatistics(
                date(fromDate),
                date(toDate),
                list(participants),
                list(words),
                statistics);

            Map<String,Object> result = future.get(); // TODO Use async servlet

            respond(req, res, (b) -> b.field(statistics.name(), result));
        }
        catch (Exception e) {
            respond(req, res, e);
        }
    }
}
