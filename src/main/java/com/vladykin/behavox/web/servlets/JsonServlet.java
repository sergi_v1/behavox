package com.vladykin.behavox.web.servlets;

import com.vladykin.behavox.util.ConsumerIO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.common.xcontent.XContentBuilder;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

public class JsonServlet extends HttpServlet {

    private static final String START_TIME = JsonServlet.class.getSimpleName() + ".start_time";

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // log level INFO
        log("Request from " + req.getRemoteHost() + ": " + getFullUrl(req));
        req.getSession().setAttribute(START_TIME, System.nanoTime());
        super.service(req, resp);
    }

    private static String getFullUrl(HttpServletRequest request) {
        String requestUrl = request.getRequestURL().toString();
        String queryString = request.getQueryString();

        return queryString == null ? requestUrl : requestUrl + '?' + queryString;
    }

    void respond(HttpServletRequest req, HttpServletResponse res, Exception err) {
        respond(req, res, err, null);
    }

    void respond(HttpServletRequest req, HttpServletResponse res, ConsumerIO<XContentBuilder> ok) {
        respond(req, res, null, ok);
    }

    private void respond(
        HttpServletRequest req,
        HttpServletResponse res,
        Exception err,
        ConsumerIO<XContentBuilder> ok
    ) {
        HttpSession ses = req.getSession();
        Long start = (Long)ses.getAttribute(START_TIME);
        ses.removeAttribute(START_TIME);

        try {
            XContentBuilder b = jsonBuilder();

            b.startObject();
            b.field("status", err == null ? "OK" : "ERROR");

            if (err != null) {
                log("Servlet failed.", err);

                b.field("errorType", err.getClass().getName());

                String msg = err.getMessage();

                if (msg != null)
                    b.field("errorMessage", msg);
            }
            else {
                b.field("result");
                b.startObject();
                ok.accept(b);
                b.endObject();
            }

            if (start != null)
                b.field("elapsedNanos", System.nanoTime() - start);

            b.endObject();

            BytesReference responseBytes = b.bytes();

            res.setContentType("application/json");
            responseBytes.writeTo(res.getOutputStream());
        }
        catch (IOException e) {
            log("Failed to write response.", e);
        }
    }
}
