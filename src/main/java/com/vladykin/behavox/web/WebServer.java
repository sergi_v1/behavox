package com.vladykin.behavox.web;


import com.google.inject.servlet.GuiceFilter;
import com.google.inject.servlet.ServletModule;
import com.vladykin.behavox.web.servlets.ElasticQueryServlet;
import com.vladykin.behavox.web.servlets.UploadServlet;
import java.util.EnumSet;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.DispatcherType;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;

@Singleton
public class WebServer {

    public static final String PORT = "web_port";

    private final Server server;

    private final AtomicBoolean started = new AtomicBoolean();

    @Inject
    public WebServer(
        @Named(PORT) int port
    ) {
        this.server = new Server(port);
    }

    public void start() throws Exception {
        if (!started.compareAndSet(false, true))
            throw new IllegalStateException("Already started.");

        ServletContextHandler ctx = new ServletContextHandler(server, "/", ServletContextHandler.SESSIONS);
        ctx.setResourceBase(getClass().getClassLoader().getResource("www").toExternalForm());
        ctx.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));

        server.start();
    }

    public static class Module extends ServletModule {
        @Override
        protected void configureServlets() {
            bind(DefaultServlet.class).in(Singleton.class);
            serve("*.html", "*.css", "*.js").with(DefaultServlet.class);

            bind(UploadServlet.class);
            serve("/upload/archive").with(UploadServlet.class);

            bind(ElasticQueryServlet.class);
            serve("/elastic/query").with(ElasticQueryServlet.class);
        }
    }
}
