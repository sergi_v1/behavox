package com.vladykin.behavox.email;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;

import static java.util.Objects.requireNonNull;

/**
 * Generates JSON documents from {@link Email} objects.
 *
 * @see #buildJson(Email)
 * @see #toJsonString(Email)
 */
public class EmailJson {

    public static final String ID = "id";
    public static final String DATE = "date";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String SUBJ = "subj";
    public static final String BODY = "body";
    public static final String COMPRESS = "compress";
    public static final String ORIGINAL = "original";

    public static final String[] FIELDS = {
        ID, DATE, FROM, TO, SUBJ, BODY, COMPRESS, ORIGINAL
    };

    public static String toJsonString(Email email) throws IOException {
        return buildJson(email).string();
    }

    public static void writeJson(Email email, OutputStream out) throws IOException {
        buildJson(email).bytes().writeTo(out);
    }

    public static XContentBuilder buildJson(Email email) throws IOException {
        XContentBuilder b = XContentFactory.jsonBuilder();

        b.startObject();

        field(b, ID, requireNonNull(email.getMessageId()));
        field(b, DATE, email.getDate());

        field(b, FROM, email.getFrom());
        field(b, TO, email.getTo());

        field(b, SUBJ, email.getSubject());
        field(b, BODY, email.getBody());

        field(b, COMPRESS, email.getCompression());
        field(b, ORIGINAL, email.getOriginal());

        b.endObject();

        return b;
    }

    private static void field(XContentBuilder b, String name, String value) throws IOException {
        if (value != null)
            b.field(name, value);
    }

    private static void field(XContentBuilder b, String name, Date value) throws IOException {
        if (value != null)
            b.dateField(name, null, value.getTime());
    }

    private static void field(XContentBuilder b, String name, byte[] value) throws IOException {
        if (value != null)
            b.field(name, value, 0, value.length);
    }

    private static void field(XContentBuilder b, String name, List<String> value) throws IOException {
        if (value == null || value.isEmpty())
            return;

        b.field(name);
        b.startArray();

        for (String str : value)
            b.value(requireNonNull(str));

        b.endArray();
    }
}
