package com.vladykin.behavox.email;

import java.util.Date;
import java.util.List;

/**
 * Simplified email message POJO.
 */
public class Email {

    private String messageId;

    private List<String> from;

    private List<String> to;

    private Date date;

    private String subject;

    private String body;

    private byte[] original;

    private String compression;

    public List<String> getTo() {
        return to;
    }

    public List<String> getFrom() {
        return from;
    }

    public Date getDate() {
        return date;
    }

    public String getBody() {
        return body;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getSubject() {
        return subject;
    }

    public byte[] getOriginal() {
        return original;
    }

    public String getCompression() {
        return compression;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setOriginal(byte[] original) {
        this.original = original;
    }

    public void setCompression(String compression) {
        this.compression = compression;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setFrom(List<String> from) {
        this.from = from;
    }

    public void setTo(List<String> to) {
        this.to = to;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return
            "\nId  :  " + getMessageId() +
            "\nDate:  " + getDate() +
            "\nSubj:  " + getSubject() +
            "\nFrom:  " + getFrom() +
            "\nTo  :  " + getTo() +
            "\nBody:\n" + getBody();
    }
}
