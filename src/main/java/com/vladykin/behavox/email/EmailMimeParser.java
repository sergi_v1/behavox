package com.vladykin.behavox.email;

import com.vladykin.behavox.util.CompressingInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.james.mime4j.dom.BinaryBody;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.address.Group;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.message.DefaultMessageBuilder;
import org.apache.james.mime4j.stream.MimeConfig;

import static java.util.Objects.requireNonNull;

/**
 * Parser for MIME messages.
 *
 * @see #parse(InputStream)
 * @see #parse(InputStream, boolean, String)
 */
public class EmailMimeParser {

    private static final String[] EMAIL_SUBJECT_PREFIXES = {
        "RE:", "Re:", "re:",
        "FW:", "Fw:", "fw:",
        "FWD:", "Fwd:", "fwd:",
    };

    private static final ThreadLocal<char[]> charBuffer = ThreadLocal.withInitial(() -> new char[8 * 1024]);

    private static String body(Body body) throws IOException {
        StringBuilder sb = collectAllTextBodyContents(body, null);

        return sb == null ? null : sb.toString();
    }

    private static StringBuilder collectAllTextBodyContents(Body body, StringBuilder accum) throws IOException {
        if (body != null) {
            if (body instanceof SingleBody) {
                if (body instanceof TextBody) {
                    if (accum == null)
                        accum = new StringBuilder();
                    else
                        accum.append("\n\n");

                    copyTo(((TextBody)body).getReader(), accum);
                }
                else
                    assert body instanceof BinaryBody : body.getClass();
            }
            else if (body instanceof Multipart) {
                for (Entity entity : ((Multipart)body).getBodyParts())
                    accum = collectAllTextBodyContents(entity.getBody(), accum);
            }
            else
                throw new IllegalStateException(body.getClass().getName());
        }

        return accum;
    }

    private static void copyTo(Reader in, StringBuilder out) throws IOException {
        for (char[] buf = charBuffer.get();;) {
            int len = in.read(buf);

            if (len > 0)
                out.append(buf, 0, len);
            else if (len == -1)
                return;
        }
    }

    private static List<String> from(MailboxList from, Mailbox sender, AddressList replyTo) {
        List<String>
        list = collectAddresses(from, null);
        list = collectAddresses(sender, list);
        list = collectAddresses(replyTo, list);

        return list;
    }

    private static List<String> to(AddressList to, AddressList cc, AddressList bcc) {
        List<String>
        list = collectAddresses(to, null);
        list = collectAddresses(cc, list);
        list = collectAddresses(bcc, list);

        return list;
    }

    private static List<String> collectAddresses(Mailbox mailbox, List<String> accum) {
        if (mailbox != null) {
            if (accum == null)
                accum = new ArrayList<>();

            accum.add(requireNonNull(mailbox.getAddress()));
        }

        return accum;
    }

    private static List<String> collectAddresses(MailboxList addrs, List<String> accum) {
        if (addrs != null && addrs.size() > 0) {
            for (Mailbox mailbox : addrs)
                accum = collectAddresses(mailbox, accum);
        }

        return accum;
    }

    private static List<String> collectAddresses(AddressList addrs, List<String> accum) {
        if (addrs != null && addrs.size() > 0) {
            for (Address addr : addrs) {
                if (addr instanceof Group)
                    accum = collectAddresses(((Group)addr).getMailboxes(), accum);
                else
                    accum = collectAddresses((Mailbox)addr, accum);
            }
        }

        return accum;
    }

    private static String subject(String subj) {
        if (subj == null)
            return null;

        outer: for (;;) {
            subj = subj.trim();

            for (String prefix : EMAIL_SUBJECT_PREFIXES) {
                if (subj.startsWith(prefix)) {
                    subj = subj.substring(prefix.length());
                    continue outer;
                }
            }

            return subj;
        }
    }

    /**
     * @param mime MIME Message input stream.
     * @return Email POJO.
     * @throws IOException If failed.
     */
    public static Email parse(InputStream mime) throws IOException {
        return parse(mime, false, null);
    }

    /**
     * @param mime MIME Message input stream.
     * @param compressionMethod Compression method from {@link CompressorStreamFactory} for preserved original message bytes.
     * @return Email POJO.
     * @throws IOException If failed.
     */
    public static Email parse(InputStream mime, String compressionMethod) throws IOException {
        requireNonNull(compressionMethod);
        return parse(mime, true, compressionMethod);
    }

    /**
     * @param mime MIME Message input stream.
     * @param preserveOriginal Whether the original message bytes must be available with {@link Email#getOriginal()}.
     * @param compressionMethod Compression method from {@link CompressorStreamFactory} for preserved original message bytes
     *                          or {@code null} for no compression.
     * @return Email POJO.
     * @throws IOException If failed.
     */
    public static Email parse(InputStream mime, boolean preserveOriginal, String compressionMethod) throws IOException {
        requireNonNull(mime);

        if (preserveOriginal)
            mime = new CompressingInputStream(mime, compressionMethod);

        DefaultMessageBuilder builder = new DefaultMessageBuilder();

        builder.setMimeEntityConfig(new MimeConfig.Builder()
            .setMaxLineLen(100_000)
            .setMaxHeaderLen(100_000)
            .build());

        Message msg = builder.parseMessage(mime);

        Email result = new Email();

        if (preserveOriginal) {
            result.setOriginal(((CompressingInputStream)mime).getCompressedBytes());
            result.setCompression(compressionMethod);
        }

        // TODO handle message id absence??
        result.setMessageId(requireNonNull(msg.getMessageId(), "Message ID must not be null."));
        result.setFrom(from(msg.getFrom(), msg.getSender(), msg.getReplyTo()));
        result.setTo(to(msg.getTo(), msg.getCc(), msg.getBcc()));
        result.setDate(msg.getDate());
        result.setSubject(subject(msg.getSubject()));
        result.setBody(body(msg.getBody()));

        return result;
    }

    private EmailMimeParser() {}
}
