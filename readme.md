Behavox
=======

Setup
-----
- Start ElasticSearch locally on port 9200.

- Create ElasticSearch index:
```bash
curl -XDELETE '127.0.0.1:9200/email'
curl -XPUT '127.0.0.1:9200/email?pretty' -H 'Content-Type: application/json' -d'
{
 "mappings": {
  "email": {
   "properties": {
    "id"  : {"type": "keyword"},
    "date": {"type": "date", "format": "epoch_millis"},
    "from": {"type": "keyword"},
    "to"  : {"type": "keyword"},
    "subj": {"type": "text", "fielddata": true},
    "body": {"type": "text", "fielddata": true},
    "compress": {"type": "keyword"},
    "original": {"type": "binary"}
   }
  }
 }
}
'
```
- Start application with `com.vladykin.behavox.Application` class.

- Upload Enron email archive either from web browser by URL `http://127.0.0.1:8080/upload.html` or with `curl`:
```bash
curl -XPUT --data-binary '@enron_mail_20150507.tar.gz' '127.0.0.1:8080/upload/archive' 
```
Result will look like:
```json
{"status":"OK","result":{"storedEmails":517401},"elapsedNanos":244207382389}
``` 
- Run some queries from browser like `http://127.0.0.1:8080/elastic/query?stat=AVG_BODY_LENGTH&fromDate=11.11.1999&participants=morela.hernandez@enron.com`



